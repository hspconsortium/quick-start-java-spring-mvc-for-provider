package org.hspconsortium.example.client.stu3;

import org.apache.commons.lang.StringUtils;
import org.hl7.fhir.dstu3.model.DateTimeType;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hspconsortium.client.auth.credentials.ClientSecretCredentials;
import org.hspconsortium.client.session.Session;
import org.hspconsortium.client.session.authorizationcode.AuthorizationCodeSessionFactory;
import org.hspconsortium.example.client.model.MyHeight;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Controller
@Profile("stu3")
public class WelcomeController {

    @Inject
    private AuthorizationCodeSessionFactory<ClientSecretCredentials> ehrSessionFactory;

    @RequestMapping(value = "/welcome", method = RequestMethod.GET)
    public String patientHeightChart(HttpSession httpSession, Model model) {
        // retrieve the EHR session from the http session
        Session ehrSession = (Session) httpSession.getAttribute(ehrSessionFactory.getSessionKey());

        Collection<Observation> heightObservations = ehrSession
                .getContextSTU3()
                .getPatientContext()
                .find(Observation.class, Observation.CODE.exactly().identifier("8302-2"));

        Patient patient = ehrSession.getContextSTU3().getPatientResource();

        model.addAttribute("title", "STU3 Example");

        model.addAttribute("patientFullName", StringUtils.join(patient.getName().get(0).getGiven(), " ") + " " +
                patient.getName().get(0).getFamily());

        List<MyHeight> heights = new ArrayList<>();
        for (Observation heightObservation : heightObservations) {
            String date = ((DateTimeType)heightObservation.getEffective()).getValue().toString();
            String height = ((Quantity)heightObservation.getValue()).getValue().toString();
            heights.add(new MyHeight(height, date));
        }

        model.addAttribute("heights", heights);
        return "example";
    }

}
